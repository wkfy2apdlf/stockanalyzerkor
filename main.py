# Import Module
import sys

from PyQt5.QtWidgets import QApplication

# Import Custom Module
from Module.Common.customLog import loggerSet
from Module.QSignalSlot import QSignalSlotClass
from Module.AppMain import AppMainClass


# Define Logger
log = loggerSet.setLog(__name__)


# Main Function
if __name__ == "__main__":
    # Initialize QApplication
    appQt = QApplication(sys.argv)

    # Initialize AppMain
    app = AppMainClass()
    qss = QSignalSlotClass(app)

    # Run QApplication
    appQt.exec_()
    log.debug("Main killed")
