# Import Custom Module
if __name__ == "__main__":
    from Common.customLog import loggerSet
    from AppMain import AppMainClass
    from GUI import WindowClass
elif __name__.startswith("Module"):
    from Module.Common.customLog import loggerSet
    from Module.AppMain import AppMainClass
    from Module.GUI import WindowClass
else:
    from Common.customLog import loggerSet
    from AppMain import AppMainClass
    from GUI import WindowClass


# Define Logger
log = loggerSet.setLog(__name__)


# Define QSignalSlot
class QSignalSlotClass():
    # Method
    def __init__(self, selfAppMain: AppMainClass):
        log.info("[CALL]")
        self._init_event_self_GUI(selfAppMain.myWindow)
        self._init_event_GUI(selfAppMain)
        self._init_event_AppSetting(selfAppMain)
        log.info("[FINS]")

    # GUI
    def _init_event_self_GUI(self, smw: WindowClass):
        log.info("[CALL]")
        smw.QPBLoadFile.clicked.connect(smw._QPB_LoadFile_clicked)
        log.debug("GUI event connected")
        log.info("[FINS]")

    def _init_event_GUI(self, sam: AppMainClass):
        log.info("[CALL]")
        sam.myWindow.QS_loadFileSelect.connect(sam._appSetting.set_loadFile)
        log.debug("GUI event connected")
        log.info("[FINS]")

    # AppSetting
    def _init_event_AppSetting(self, sam: AppMainClass):
        log.info("[CALL]")
        sam._appSetting.QS_loadFileSet.connect(sam.myWindow.QLE_setText)
        sam._appSetting.QS_loadFileSet.connect(sam.import_loadFile)
        log.debug("AppSetting event connected")
        log.info("[FINS]")


# Main Function
if __name__ == "__main__":
    # Create Instance
    testAppMain = AppMainClass()
    testQss = QSignalSlotClass(testAppMain)
    log.debug(f"qssSta  : {testQss._get_qssSta()}")
    testQss._set_qssSta(20)
    log.debug(f"qssSta : {testQss._get_qssSta()}")
    log.debug("QSignalSlot killed")
