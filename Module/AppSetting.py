# Import Module
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

# Import Custom Module
if __name__ == "__main__":
    from Common.customLog import loggerSet
elif __name__.startswith("Module"):
    from Module.Common.customLog import loggerSet
else:
    from Common.customLog import loggerSet


# Define Logger
log = loggerSet.setLog(__name__)


# Define AppMain
class AppSetttingClass(QObject):
    # QSignal
    QS_loadFileSet = pyqtSignal(bool, str)      # LoadFile Updated

    # Method
    def __init__(self):
        log.info("[CALL]")
        super().__init__()
        self._loadFile = ""
        log.info("[FINS]")

    def get_loadFile(self) -> str:
        log.info("[CALL]")
        log.debug(f"loadFile : {self._loadFile}")
        log.info("[FINS]")
        return self._loadFile

    @pyqtSlot(bool, str)
    # myWindow.QS_loadFileSelect
    def set_loadFile(self, dataSta: bool, newFile: str) -> bool:
        log.info("[CALL]")
        funSta = False
        log.debug(f"Input : {newFile} - {dataSta}")
        if dataSta:
            funSta = True
            self._loadFile = newFile
            log.debug(f"loadFile : {self.get_loadFile()}")
            log.debug("QS_loadFileSet emitted!!!")
            self.QS_loadFileSet.emit(funSta, newFile)
        else:
            funSta = False
            log.debug("Input data is not valid")
        log.debug(f"funSta : {funSta}")
        log.info("[FINS]")
        return funSta


# Main Function
if __name__ == "__main__":
    # Create Instance
    testAppSetting = AppSetttingClass()
    testAppSetting.get_loadFile()
    testAppSetting.set_loadFile(True, "TestFile.ini")
    log.debug("AppSettting killed")
