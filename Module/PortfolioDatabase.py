# Import Custom Module
if __name__ == "__main__":
    from Common.customLog import loggerSet
    from Common.GeneralDatabase import BaseDatabaseClass
    from StockDatabase import StockDatabaseClass
elif __name__.startswith("Module"):
    from Module.Common.customLog import loggerSet
    from Module.Common.GeneralDatabase import BaseDatabaseClass
    from Module.StockDatabase import StockDatabaseClass
else:
    from Common.customLog import loggerSet
    from Common.GeneralDatabase import BaseDatabaseClass
    from StockDatabase import StockDatabaseClass


# Define Logger
log = loggerSet.setLog(__name__)


# Define PfDatabase
class PfDatabaseClass(BaseDatabaseClass):
    # Constant
    # _MAX_OBJSTA
    #   0b001:_loadFile
    #   0b010:_subDB
    #   0b100:_selKey
    _MAX_OBJSTA = 0b111

    # Attribute
    _depthSubDB = 1
    _prefixSubDB = ""

    # Method
    def __init__(self):
        log.info("[CALL]")
        super().__init__()
        self._set_subDB("SummaryStock", self._get_objSubDB())
        self._set_selKey(True, "SummaryStock")
        log.info("[FINS]")

    def _get_objSubDB(self) -> StockDatabaseClass:
        log.info("[CALL]")
        object = StockDatabaseClass()
        log.info("[FINS]")
        return object


# Main Function
if __name__ == "__main__":
    # Create Instance
    testPfDB = PfDatabaseClass()

    # Test import_loadFile
    log.debug("[TEST] import_loadFile")
    testPfDB.import_loadFile(
        True,
        "F:/Dropbox/Codes/02_Private Git/"
        + "54_PyQt5_GUI/Data/user_ADMIN/pfolio_DEFAULT/"
        + "000660_SK하이닉스_211024.xlsx"
    )
    log.debug(f"_loadFile : {testPfDB._get_loadFile()}")
    log.debug(f"_selKey : {testPfDB._get_selKey()}")
    log.debug(f"_subDB : {testPfDB._get_subDB()}")

    log.debug("[TEST] import_loadFile - exception")
    testPfDB.import_loadFile(
        True,
        "F:/Dropbox/Codes/02_Private Git/"
        + "54_PyQt5_GUI/Data/"
        + "000660_SK하이닉스_211024.xlsx"
    )
    log.debug(f"_loadFile : {testPfDB._get_loadFile()}")
    log.debug(f"_selKey : {testPfDB._get_selKey()}")
    log.debug(f"_subDB : {testPfDB._get_subDB()}")

    log.debug("PortfolioDatabase killed")
