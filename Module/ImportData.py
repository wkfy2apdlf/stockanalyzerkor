# Import Module
import pandas as pd

# Import Custom Module
if __name__ == "__main__":
    from Common.customLog import loggerSet
    from Common.GeneralObject import BaseObjectClass
elif __name__.startswith("Module"):
    from Module.Common.customLog import loggerSet
    from Module.Common.GeneralObject import BaseObjectClass
else:
    from Common.customLog import loggerSet
    from Common.GeneralObject import BaseObjectClass


# Define Logger
log = loggerSet.setLog(__name__)


# Define ImportData
class ImportDataClass(BaseObjectClass):
    # Method
    def __init__(self):
        super().__init__()
        self._fileName: str = ""
        self._rawPanda: pd = {}

    def import_xls(self, fileName) -> bool:
        log.info("[CALL]")
        log.debug(f"fileName : {fileName}")

        self._rawPanda = pd.read_excel(fileName)
        print(self._rawPanda)
        log.info("[FINS]")
        return True


# Main Function
if __name__ == "__main__":
    # Create Instance
    testIpData = ImportDataClass()
    testIpData.import_xls("./Data/000660_SK하이닉스_211024.xlsx")
    log.debug("ImportData killed")
