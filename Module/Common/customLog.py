# Import Module
import logging
import logging.config

# Import Custom Module
if __name__ == "__main__":
    import constants as CONST
elif __name__.startswith("Module"):
    import Module.Common.constants as CONST
elif __name__.startswith("Common"):
    import Common.constants as CONST
else:
    import constants as CONST


# Define loggerSet
class loggerSet():
    def setLog(logName: str) -> logging:
        logging.config.fileConfig('Module/Common/customLog.conf')
        # Check DEBUG_ENABLE
        if CONST.DEBUG_ENABLE:
            logger = logging.getLogger(logName)
        else:
            logger = logging.getLogger("Default")
        logger.debug("DEBUG_ENABLE: " + str(CONST.DEBUG_ENABLE))
        logger.debug("Logger configured")
        return logger


# Main Function
if __name__ == "__main__":
    # Create logger
    log = loggerSet.setLog(__name__)

    # Test Logger
    log.debug('debug message')
    log.info('info message')
    log.warning('warn message')
    log.error('error message')
    log.critical('critical message')
