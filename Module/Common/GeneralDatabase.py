# Import Custom Module
from pathlib import PurePath

# Import Custom Module
if __name__ == "__main__":
    from customLog import loggerSet
    from GeneralObject import BaseObjectClass
elif __name__.startswith("Module"):
    from Module.Common.customLog import loggerSet
    from Module.Common.GeneralObject import BaseObjectClass
elif __name__.startswith("Common"):
    from Common.customLog import loggerSet
    from Common.GeneralObject import BaseObjectClass
else:
    from GeneralObject import BaseObjectClass


# Define Logger
log = loggerSet.setLog(__name__)


# Define BaseDatabase
class BaseDatabaseClass(BaseObjectClass):
    # Attribute
    _depthSubDB: int = 0
    _prefixSubDB: str = "base_"

    # Method
    def __init__(self):
        log.info("[CALL]")
        super().__init__()
        self._loadFile: str = ""    # _objSta 0b001
        self._subDB: dict = {}      # _objSta 0b010
        self._selKey: str = ""      # _objSta 0b100
        log.info("[FINS]")

    def _get_objSubDB(self) -> int:
        log.info("[CALL]")
        object = int()
        log.info("[FINS]")
        return object

    def _get_loadFile(self) -> str:
        log.info("[CALL]")
        log.debug(f"_loadFile : {self._loadFile}")
        log.info("[FINS]")
        return self._loadFile

    def _set_loadFile(self, dataSta: int, newFile: str) -> bool:
        log.info("[CALL]")
        funSta = False
        log.debug(f"Input : {newFile} - {dataSta}")
        if dataSta:
            self._loadFile = newFile
            funSta = True
            self._set_biton_objSta(0b001)
            log.debug(f"_loadFile : {self._get_loadFile()}")
        else:
            funSta = False
            log.debug("Input is not valid")
        log.info("[FINS]")
        return funSta

    def _clear_loadFile(self) -> bool:
        self._loadFile = ""
        return True

    def _get_subDB(self) -> dict:
        log.info("[CALL]")
        log.debug(f"_subDB : {self._subDB}")
        log.info("[FINS]")
        return self._subDB

    def _set_subDB(self, newKey: str, newVal: int) -> bool:
        log.info("[CALL]")
        funSta = False
        log.debug(f"Input newKey : {newKey}, newVal : {newVal}")
        if self._get_subDB_value(newKey) is not None:
            log.debug(f"_subDB updated - {newKey}")
        else:
            log.debug(f"_subDB appended - {newKey}")
        self._subDB[newKey] = newVal
        funSta = True
        self._set_biton_objSta(0b010)
        log.info("[FINS]")
        return funSta

    def _del_subDB(self, targetKey: str) -> bool:
        log.info("[CALL]")
        funSta = False
        log.debug(f"Input targetKey : {targetKey}")
        if self._get_subDB_value(targetKey):
            funSta = True
            del self._subDB[targetKey]
            log.debug("targetKey-Value deleted")
            if not self._subDB:
                self._set_bitoff_objSta(0b010)
                log.debug("_subDB empty")
        else:
            funSta = False
            log.debug("No targetKey is found in _subDB : None")
        log.info("[FINS]")
        return funSta

    def _clear_subDB(self) -> bool:
        self._subDB = {}
        return True

    def _get_subDB_value(self, targetKey: str) -> int:
        log.info("[CALL]")
        log.debug(f"Input targetKey : {targetKey}")
        targetValue = None
        if (self._subDB) and (targetKey in self._subDB):
            targetValue = self._subDB[targetKey]
            log.debug(f"targetValue : {targetValue}")
        else:
            log.debug("No targetKey is found in _subDB : None")
        log.info("[FINS]")
        return targetValue

    def _get_selKey(self) -> str:
        log.info("[CALL]")
        log.debug(f"_selKey : {self._selKey}")
        log.info("[FINS]")
        return self._selKey

    def _set_selKey(self, dataSta: bool, newStr: str) -> bool:
        log.info("[CALL]")
        funSta = False
        log.debug(f"Input newStr : {newStr}")
        if dataSta and (newStr in self._subDB):
            self._selKey = newStr
            funSta = True
            self._set_biton_objSta(0b100)
            log.debug(f"_selKey : {self._get_selKey()}")
        else:
            funSta = False
            log.debug("Input key is not in DB")
        log.info("[FINS]")
        return funSta

    def _clear_selKey(self) -> bool:
        self._selKey = ""
        return True

    def _gen_selKey(self, dataSta: bool, newPath: str) -> str:
        log.info("[CALL]")
        retStr = None
        log.debug(f"Input newPath : {newPath}")
        if dataSta:
            dirPart = PurePath(newPath).parts
            log.debug(f"dirPart : {dirPart}")
            if dirPart:
                # Valid Path
                findIdx = []
                for idx, dir in enumerate(dirPart):
                    if dir.startswith(self._prefixSubDB):
                        findIdx.append(idx)
                if findIdx:
                    # _subDB Prefix Matched
                    keyIdx = max(findIdx)
                    log.debug(f"keyIdx : {keyIdx}")
                    if dirPart[keyIdx] == self._prefixSubDB:
                        # W/ _subDB Prefix Empty Name
                        keyStr = "IMPORT"
                    else:
                        # W/ _subDB Prefix Valid Name
                        keyStr = dirPart[keyIdx].replace(self._prefixSubDB, "")
                else:
                    # _subDB Prefix not Matched
                    if len(dirPart) > self._depthSubDB:
                        # Valid Path Depth
                        keyStr = dirPart[-(self._depthSubDB + 1)]
                    else:
                        # Invalid Path Depth
                        keyStr = "IMPORT"
                if self._depthSubDB < 2:
                    retStr = PurePath(keyStr).stem.split("_")[1]
                else:
                    retStr = keyStr
            else:
                # Invalid Path
                retStr = None
                log.debug("Input path is not valid")
        else:
            retStr = None
            log.debug("Input path is not valid")
        log.debug(f"Generated selKey - {retStr}")
        log.info("[FINS]")
        return retStr

    def clear_all(self) -> bool:
        self._clear_loadFile()
        self._clear_subDB()
        self._clear_selKey()
        return True

    def import_loadFile(self, dataSta: bool, newFile: str) -> bool:
        log.info("[CALL]")
        funSta = False
        lfSta = self._set_loadFile(dataSta, newFile)
        newKey = self._gen_selKey(lfSta, newFile)
        if newKey is not None:
            # Valid Path
            if self._get_subDB_value(newKey) is not None:
                log.debug("newKey exists in _subDB")
            else:
                self._set_subDB(newKey, self._get_objSubDB())
                log.debug("newKey appended to _subDB")
            self._set_selKey(self._get_subDB_value(newKey) is not None, newKey)
            if self._depthSubDB == 0:
                log.debug("import_loadFile bottom reached")
            else:
                self._subDB[newKey].import_loadFile(dataSta, newFile)
            funSta = True
        else:
            # Invalid Path
            funSta = False
            log.debug("Input newFile cannot be _loadFile")
        log.info("[FINS]")
        return funSta


# Main Function
if __name__ == "__main__":
    # Create Instance
    log.debug("[TEST] __init__")
    testDB = BaseDatabaseClass()

    # Test _loadFile
    log.debug("[TEST] _loadFile")
    log.debug(f"_loadFile : {testDB._get_loadFile()}")
    log.debug(f"_loadFile : {testDB._get_loadFile()}")
    testDB._set_loadFile(True, "Test.ini")
    log.debug(f"_loadFile : {testDB._get_loadFile()}")

    # Test _subDB
    log.debug("[TEST] _set_subDB - append")
    testSubDB = testDB._get_subDB()
    log.debug(f"_subDB : {testSubDB}")
    testDB._set_subDB("Test1", 77)
    testSubDB = testDB._get_subDB()
    log.debug(f"_subDB : {testSubDB}")
    testDB._set_subDB("Test2", 88)
    testSubDB = testDB._get_subDB()
    log.debug(f"_subDB : {testSubDB}")

    log.debug("[TEST] _set_subDB - Update")
    testDB._set_subDB("Test1", 99)
    testSubDB = testDB._get_subDB()
    log.debug(f"_subDB : {testSubDB}")

    log.debug("[TEST] _del_subDB - delete")
    testDB._del_subDB("Test2")
    testSubDB = testDB._get_subDB()
    log.debug(f"_subDB : {testSubDB}")

    log.debug("[TEST] _del_subDB - error")
    testDB._del_subDB("Test3")
    testSubDB = testDB._get_subDB()
    log.debug(f"_subDB : {testSubDB}")

    log.debug("[TEST] _get_subDB_value")
    testValue = testDB._get_subDB_value("Test1")
    log.debug(f"Value : {testValue}")

    log.debug("[TEST] _get_subDB_value - error")
    testValue = testDB._get_subDB_value("Test2")
    log.debug(f"Value : {testValue}")

    # Test _selKey
    log.debug("[TEST] _selKey")
    log.debug(f"_selKey : {testDB._get_selKey()}")
    resSta = testDB._set_selKey(True, "Test1")
    log.debug(f"_selKey : {testDB._get_selKey()}")

    log.debug("[TEST] _selKey - error")
    testDB._set_selKey(True, "Test2")
    log.debug(f"_selKey : {testDB._get_selKey()}")

    # Test import_loadFile
    log.debug("[TEST] import_loadFile")
    testDB.import_loadFile(
        True,
        "F:/Dropbox/Codes/02_Private Git/"
        + "54_PyQt5_GUI/Data/user_ADMIN/pfolio_DEFAULT/"
        + "000660_SK하이닉스_211024.xlsx"
    )
    log.debug(f"_loadFile : {testDB._get_loadFile()}")
    log.debug(f"_selKey : {testDB._get_selKey()}")
    log.debug(f"_subDB : {testDB._get_subDB()}")

    log.debug("Database killed")
