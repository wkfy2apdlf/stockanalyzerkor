# Import Module
from typing import Final

# Debug
DEBUG_ENABLE: Final = True

# BaseObjectClass
MAX_OBJSTA: Final = 0b111
