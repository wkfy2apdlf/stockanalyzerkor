# Import Custom Module
if __name__ == "__main__":
    import constants as CONST
    from customLog import loggerSet
elif __name__.startswith("Module"):
    import Module.Common.constants as CONST
    from Module.Common.customLog import loggerSet
elif __name__.startswith("Common"):
    import Common.constants as CONST
    from Common.customLog import loggerSet
else:
    import constants as CONST
    from customLog import loggerSet


# Define Logger
log = loggerSet.setLog(__name__)


# Define BaseObject
class BaseObjectClass():
    # Attribute
    _MAX_OBJSTA: int = CONST.MAX_OBJSTA

    # Method
    def __init__(self):
        log.info("[CALL]")
        super().__init__()
        self._objSta = -1
        self._set_objSta(0)
        log.info("[FINS]")

    def _get_objSta(self) -> int:
        log.info("[CALL]")
        log.debug(f"_objSta : {bin(self._objSta)}")
        log.info("[FINS]")
        return self._objSta

    def _set_objSta(self, newSta: int) -> bool:
        log.info("[CALL]")
        log.debug(f"Input newSta : {bin(newSta)}")
        if newSta > self._MAX_OBJSTA:
            funSta = False
            log.debug(f"newSta over maximum-{newSta}/{self._MAX_OBJSTA}")
        else:
            self._objSta = newSta
            funSta = True
            log.debug(f"_objSta : {bin(self._get_objSta())}")
        log.info("[FINS]")
        return funSta

    def _set_biton_objSta(self, onBit: int) -> bool:
        log.info("[CALL]")
        funSta = False
        log.debug(f"Input onBit : {bin(onBit)}")
        if onBit > self._MAX_OBJSTA:
            funSta = False
            log.debug(f"onBit over maximum-{onBit}/{self._MAX_OBJSTA}")
        else:
            self._set_objSta(onBit | self._get_objSta())
            funSta = True
            log.debug(f"_objSta : {bin(self._get_objSta())}")
        log.info("[FINS]")
        return funSta

    def _set_bitoff_objSta(self, offBit: int) -> bool:
        log.info("[CALL]")
        funSta = False
        log.debug(f"Input offBit : {bin(offBit)}")
        if offBit > self._MAX_OBJSTA:
            funSta = False
            log.debug(f"offBit over maximum-{offBit}/{self._MAX_OBJSTA}")
        else:
            self._set_objSta((offBit ^ self._MAX_OBJSTA) & self._get_objSta())
            funSta = True
            log.debug(f"_objSta : {bin(self._get_objSta())}")
        log.debug(f"_objSta : {bin(self._get_objSta())}")
        log.info("[FINS]")
        return funSta


# Main Function
if __name__ == "__main__":
    # Create Instance
    testObject = BaseObjectClass()

    # Test _objSta
    log.debug("[TEST] _set_objSta")
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_objSta(7)
    log.debug(f"_objSta : {testObject._get_objSta()}")

    log.debug("[TEST] _set_objSta - over maximum range")
    testObject._set_objSta(0xFFFF)
    log.debug(f"_objSta : {testObject._get_objSta()}")

    log.debug("[TEST] _set_biton_objSta")
    testObject._set_objSta(0)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_biton_objSta(0b010)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_biton_objSta(0b100)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_biton_objSta(0b110)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_biton_objSta(0b001)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    log.debug("[TEST] _set_biton_objSta - over maximum range")
    testObject._set_objSta(0)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_biton_objSta(0xFFFF)
    log.debug(f"_objSta : {testObject._get_objSta()}")

    log.debug("[TEST] _set_bitoff_objSta")
    testObject._set_objSta(0b111)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_bitoff_objSta(0b010)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_bitoff_objSta(0b100)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_bitoff_objSta(0b110)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_bitoff_objSta(0b001)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    log.debug("[TEST] _set_bitoff_objSta - over maximum range")
    testObject._set_objSta(0b111)
    log.debug(f"_objSta : {testObject._get_objSta()}")
    testObject._set_bitoff_objSta(0xFFFF)
    log.debug(f"_objSta : {testObject._get_objSta()}")

    log.debug("Database killed")
