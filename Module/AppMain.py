# Import Module
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

# Import Custom Module
if __name__ == "__main__":
    from Common.customLog import loggerSet
    from Common.GeneralDatabase import BaseDatabaseClass
    from GUI import WindowClass
    from AppSetting import AppSetttingClass
    from AppDatabase import AppDatabaseClass
elif __name__.startswith("Module"):
    from Module.Common.customLog import loggerSet
    from Module.Common.GeneralDatabase import BaseDatabaseClass
    from Module.GUI import WindowClass
    from Module.AppSetting import AppSetttingClass
    from Module.AppDatabase import AppDatabaseClass
else:
    from Common.customLog import loggerSet
    from Common.GeneralDatabase import BaseDatabaseClass
    from GUI import WindowClass
    from AppSetting import AppSetttingClass
    from AppDatabase import AppDatabaseClass


# Define Logger
log = loggerSet.setLog(__name__)


# Define AppMain
class AppMainClass(QObject, BaseDatabaseClass):
    # Constant
    # _MAX_OBJSTA
    #   0b001:_loadFile
    #   0b010:_subDB
    #   0b100:_selKey
    _MAX_OBJSTA = 0b111

    # Attribute
    _depthSubDB = 3
    _prefixSubDB = "user_"

    # Method
    def __init__(self):
        log.info("[CALL]")
        super().__init__()
        self._init_GUI()
        self._init_app()
        self._set_subDB("ADMIN", self._get_objSubDB())
        self._set_selKey(True, "ADMIN")
        log.info("[FINS]")

    def _get_objSubDB(self) -> AppDatabaseClass:
        log.info("[CALL]")
        object = AppDatabaseClass()
        log.info("[FINS]")
        return object

    def _init_GUI(self) -> bool:
        log.info("[CALL]")
        self.myWindow = WindowClass()
        self.myWindow.show()
        log.info("[FINS]")
        return True

    def _init_app(self) -> bool:
        log.info("[CALL]")
        self._appSetting = AppSetttingClass()
        log.info("[FINS]")
        return True

    @pyqtSlot(bool, str)
    # _appSetting.QS_loadFileSet
    def import_loadFile(self, dataSta: bool, newFile: str) -> bool:
        log.info("[CALL]")
        funSta = super().import_loadFile(dataSta, newFile)
        log.info("[FINS]")
        return funSta


# Main Function
if __name__ == "__main__":
    # Create Instance
    testAppMain = AppMainClass()
    log.debug("AppMain killed")
