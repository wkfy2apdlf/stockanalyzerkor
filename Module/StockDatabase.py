# Import Module
import numpy as np

# Import Custom Module
if __name__ == "__main__":
    from Common.customLog import loggerSet
    from Common.GeneralDatabase import BaseDatabaseClass
    from ImportData import ImportDataClass
elif __name__.startswith("Module"):
    from Module.Common.customLog import loggerSet
    from Module.Common.GeneralDatabase import BaseDatabaseClass
    from Module.ImportData import ImportDataClass
else:
    from Common.customLog import loggerSet
    from Common.GeneralDatabase import BaseDatabaseClass
    from ImportData import ImportDataClass


# Define Logger
log = loggerSet.setLog(__name__)


# Define StockDatabase
class StockDatabaseClass(BaseDatabaseClass):
    # Constant
    # _MAX_OBJSTA
    #   0b001:_loadFile
    #   0b010:_subDB
    #   0b100:_selKey
    _MAX_OBJSTA = 0b111

    # Attribute
    _depthSubDB = 0
    _prefixSubDB = ""

    # Method
    def __init__(self):
        log.info("[CALL]")
        super().__init__()
        self._set_subDB("Summary", self._get_objSubDB())
        self._set_selKey(True, "Summary")
        log.info("[FINS]")

    def _get_objSubDB(self) -> int:
        log.info("[CALL]")
        object = int()
        log.info("[FINS]")
        return object

    def import_loadFile(self, dataSta: bool, newFile: str) -> bool:
        log.info("[CALL]")
        funSta = False
        lfSta = self._set_loadFile(dataSta, newFile)
        if lfSta:
            self._importData = ImportDataClass()
            self._importData.import_xls(newFile)
            funSta = True
        else:
            # Invalid Path
            funSta = False
            log.debug("Input newFile cannot be _loadFile")
        log.info("[FINS]")
        return funSta


# Main Function
if __name__ == "__main__":
    # Create Instance
    testStockDB = StockDatabaseClass()

    # Test import_loadFile
    log.debug("[TEST] import_loadFile")
    testStockDB.import_loadFile(
        True,
        "F:/Dropbox/Codes/02_Private Git/"
        + "54_PyQt5_GUI/Data/user_ADMIN/pfolio_DEFAULT/"
        + "000660_SK하이닉스_211024.xlsx"
    )
    log.debug(f"_loadFile : {testStockDB._get_loadFile()}")
    log.debug(f"_selKey : {testStockDB._get_selKey()}")
    log.debug(f"_subDB : {testStockDB._get_subDB()}")

    log.debug("[TEST] import_loadFile - exception")
    testStockDB.import_loadFile(
        True,
        "F:/Dropbox/Codes/02_Private Git/"
        + "54_PyQt5_GUI/Data/"
        + "000660_SK하이닉스_211024.xlsx"
    )
    log.debug(f"_loadFile : {testStockDB._get_loadFile()}")
    log.debug(f"_selKey : {testStockDB._get_selKey()}")
    log.debug(f"_subDB : {testStockDB._get_subDB()}")

    log.debug("StockDatabase killed")
