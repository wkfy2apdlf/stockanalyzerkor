# Import Custom Module
if __name__ == "__main__":
    from Common.customLog import loggerSet
    from Common.GeneralDatabase import BaseDatabaseClass
    from PortfolioDatabase import PfDatabaseClass
elif __name__.startswith("Module"):
    from Module.Common.customLog import loggerSet
    from Module.Common.GeneralDatabase import BaseDatabaseClass
    from Module.PortfolioDatabase import PfDatabaseClass
else:
    from Common.customLog import loggerSet
    from Common.GeneralDatabase import BaseDatabaseClass
    from PortfolioDatabase import PfDatabaseClass


# Define Logger
log = loggerSet.setLog(__name__)


# Define AppDatabase
class AppDatabaseClass(BaseDatabaseClass):
    # Constant
    # _MAX_OBJSTA
    #   0b001:_loadFile
    #   0b010:_subDB
    #   0b100:_selKey
    _MAX_OBJSTA = 0b111

    # Attribute
    _depthSubDB = 2
    _prefixSubDB = "pfolio_"

    # Method
    def __init__(self):
        log.info("[CALL]")
        super().__init__()
        self._set_subDB("SummaryPf", self._get_objSubDB())
        self._set_selKey(True, "SummaryPf")
        log.info("[FINS]")

    def _get_objSubDB(self) -> PfDatabaseClass:
        log.info("[CALL]")
        object = PfDatabaseClass()
        log.info("[FINS]")
        return object


# Main Function
if __name__ == "__main__":
    # Create Instance
    testAppDB = AppDatabaseClass()

    # Test import_loadFile
    log.debug("[TEST] import_loadFile")
    testAppDB.import_loadFile(
        True,
        "F:/Dropbox/Codes/02_Private Git/"
        + "54_PyQt5_GUI/Data/user_ADMIN/pfolio_DEFAULT/"
        + "000660_SK하이닉스_211024.xlsx"
    )
    log.debug(f"_loadFile : {testAppDB._get_loadFile()}")
    log.debug(f"_selKey : {testAppDB._get_selKey()}")
    log.debug(f"_subDB : {testAppDB._get_subDB()}")

    log.debug("[TEST] import_loadFile - exception")
    testAppDB.clear_all()
    testAppDB.import_loadFile(
        True,
        "F:/Dropbox/Codes/02_Private Git/"
        + "54_PyQt5_GUI/Data/000660_SK하이닉스_211024.xlsx"
    )
    log.debug(f"_loadFile : {testAppDB._get_loadFile()}")
    log.debug(f"_selKey : {testAppDB._get_selKey()}")
    log.debug(f"_subDB : {testAppDB._get_subDB()}")

    log.debug("[TEST] import_loadFile - level")
    testAppDB.clear_all()
    testAppDB.import_loadFile(
        True,
        "F:/Dropbox/Codes/02_Private Git/"
        + "54_PyQt5_GUI/Data/01_TOP/02_SECOND/03_THIRD/"
        + "000660_SK하이닉스_211024.xlsx"
    )
    log.debug(f"_loadFile : {testAppDB._get_loadFile()}")
    log.debug(f"_selKey : {testAppDB._get_selKey()}")
    log.debug(f"_subDB : {testAppDB._get_subDB()}")
