# Import Module
import sys
import os

from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5 import uic

# Import Custom Module
if __name__ == "__main__":
    from Common.customLog import loggerSet
elif __name__.startswith("Module"):
    from Module.Common.customLog import loggerSet
else:
    from Common.customLog import loggerSet


# Define Logger
log = loggerSet.setLog(__name__)

# Define PyQt UI
form_class = uic.loadUiType("UI/MainWindow.ui")[0]
log.debug("form_class configured")


# Define WindowClass
class WindowClass(QMainWindow, form_class):
    # QSignal
    QS_loadFileSelect = pyqtSignal(bool, str)       # LoadFile Selected

    # Method
    def __init__(self):
        log.info("[CALL]")
        super().__init__()
        self.setupUi(self)
        log.info("[FINS]")

    @pyqtSlot()
    # QPBLoadFile.clicked
    def _QPB_LoadFile_clicked(self) -> bool:
        log.info("[CALL]")
        funSta = False
        fileName = QFileDialog.getOpenFileName(
            self,
            'Load file',
            directory='./',
            filter='SpreadSheet (*.xls *.xlsx)'
            )
        if fileName[0]:
            funSta = True
            log.debug(f"Selected File : {fileName[0]}")
            log.debug("QS_loadFileSelect emitted!!!")
            self.QS_loadFileSelect.emit(funSta, fileName[0])
        else:
            funSta = False
            log.debug("File is not selected")
        log.debug(f"funSta : {funSta}")
        log.info("[FINS]")
        return funSta

    @pyqtSlot(bool, str)
    # _appSetting.QS_loadFileSet
    def QLE_setText(self, dataSta: bool, newStr: str) -> bool:
        log.info("[CALL]")
        log.debug(f"Input : {newStr} - {dataSta}")
        if dataSta:
            funSta = True
            self.QLELoadFile.setText(os.path.relpath(newStr))
            log.debug(f"LineText : {self.QLELoadFile.text()}")
        else:
            funSta = False
            log.debug("Input data is not valid")
        log.debug(f"funSta : {funSta}")
        log.info("[FINS]")
        return funSta


# Main Function
if __name__ == "__main__":
    # Declare QApplication
    app = QApplication(sys.argv)
    log.debug("GUI app created")

    # Create testWindow Instance
    testWindow = WindowClass()
    log.debug("GUI WindowClass initialized")

    # Show testWindow
    testWindow.show()
    log.debug("GUI WindowClass showed")

    # Excute QApplication
    app.exec_()
    log.debug("GUI app executed")
